import { Component, OnInit } from '@angular/core';
import { Parameter } from './parameter';
import { faFacebook, faTwitter, faGooglePlus, faLinkedinIn } from '@fortawesome/free-brands-svg-icons';
; @Component({
  selector: 'card16',
  templateUrl: './card16.component.html',
  styleUrls: ['./card16.component.css']
})
export class Card16Component implements OnInit {

  parameter;
  // social icons
  faFacebook = faFacebook;
  faTwitter = faTwitter;
  faGooglePlus = faGooglePlus;
  faLinkedinIn = faLinkedinIn;


  constructor() { }

  ngOnInit() {
    this.parameter = new Parameter().parameter[0];
  }

}
