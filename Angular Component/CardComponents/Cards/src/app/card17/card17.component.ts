import { Component, OnInit } from '@angular/core';
import { Parameter } from './parameter';
import { faFacebook, faTwitter, faGooglePlus, faInstagram } from '@fortawesome/free-brands-svg-icons';
@Component({
  selector: 'card17',
  templateUrl: './card17.component.html',
  styleUrls: ['./card17.component.css']
})
export class Card17Component implements OnInit {

  //card detail
  parameter;

  //social media
  faFacebook = faFacebook;
  faTwitter = faTwitter;
  faGooglePlus = faGooglePlus;
  faInstagram = faInstagram;

  constructor() { }

  ngOnInit() {
    this.parameter = new Parameter().parameter[0];
  }

}
