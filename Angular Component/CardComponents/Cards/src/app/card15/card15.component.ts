import { Component, OnInit } from '@angular/core';
import { Parameter } from './parameter';
import { faFacebook, faTwitter, faGooglePlus, faGgCircle } from '@fortawesome/free-brands-svg-icons';
@Component({
  selector: 'card15',
  templateUrl: './card15.component.html',
  styleUrls: ['./card15.component.css']
})
export class Card15Component implements OnInit {

  parameter;
  //for icon
  faFacebook = faFacebook;
  faTwitter = faTwitter;
  faGooglePlus = faGooglePlus;
  faGgCircle = faGgCircle;

  //for adding active class
  active = false;




  addActive() {

    this.active = true;
  }
  removeActive() {
    this.active = false;
  }
  constructor() { }

  ngOnInit() {
    this.parameter = new Parameter().parameter[0];
  }

}
