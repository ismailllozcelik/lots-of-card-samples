import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Card36Component } from './card36.component';

describe('Card36Component', () => {
  let component: Card36Component;
  let fixture: ComponentFixture<Card36Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Card36Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Card36Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
