import { Component, OnInit } from '@angular/core';
import {Parameter} from './parameter';
@Component({
  selector: 'card7',
  templateUrl: './card7.component.html',
  styleUrls: ['./card7.component.css']
})
export class Card7Component implements OnInit {

  parameter;
  constructor() { }

  ngOnInit() {
    this.parameter= new Parameter().parameter[0];
  }


}
