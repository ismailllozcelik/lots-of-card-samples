import { Component, OnInit } from '@angular/core';
import {Parameter} from './parameter';
@Component({
  selector: 'card4',
  templateUrl: './card4.component.html',
  styleUrls: ['./card4.component.css']
})
export class Card4Component implements OnInit {

  parameter;
  constructor() { }

  ngOnInit() {
    this.parameter=new Parameter().parameter[0];
  }

}
