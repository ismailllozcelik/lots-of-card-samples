import { Component, OnInit } from '@angular/core';
import { Parameter } from './parameter';
import { faArrowCircleLeft, faInfoCircle, faCloud, faTint, faSnowflake } from '@fortawesome/free-solid-svg-icons';
@Component({
  selector: 'card27',
  templateUrl: './card27.component.html',
  styleUrls: ['./card27.component.css']
})
export class Card27Component implements OnInit {

  parameter;
  //icons
  faArrowCircleLeft = faArrowCircleLeft;
  faInfoCircle = faInfoCircle;
  faCloud = faCloud;
  faTint = faTint;
  faSnowflake = faSnowflake;


  //turn 3D
  isFrontActive: Boolean = true;
  isBackActive: Boolean = false;
  more() {

    if (!this.isFrontActive) {
      this.isFrontActive = true;
      this.isBackActive = false;
    }
    else {
      this.isFrontActive = false;
      this.isBackActive = true;
    }

  }

  constructor() { }

  ngOnInit() {
    this.parameter = new Parameter().parameter[0];
  }

}
