import { Component, OnInit } from '@angular/core';
import { Parameter } from './parameter'
@Component({
  selector: 'card19',
  templateUrl: './card19.component.html',
  styleUrls: ['./card19.component.css']
})
export class Card19Component implements OnInit {

  parameter;
  constructor() { }

  ngOnInit() {
    this.parameter = new Parameter().parameter[0];
  }

}
