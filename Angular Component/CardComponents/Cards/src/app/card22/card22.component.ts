import { Component, OnInit } from '@angular/core';
import { Parameter } from './parameter';
@Component({
  selector: 'card22',
  templateUrl: './card22.component.html',
  styleUrls: ['./card22.component.css']
})
export class Card22Component implements OnInit {

  parameter;

  constructor() { }

  ngOnInit() {
    this.parameter = new Parameter().parameter[0];
  }

}
