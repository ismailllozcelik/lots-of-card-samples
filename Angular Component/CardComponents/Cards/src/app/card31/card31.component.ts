import { Component, OnInit } from '@angular/core';
import { Parameter } from './parameter';
import { faCheck, faTimes, faPaperPlane, faPlaneDeparture, faRocket } from '@fortawesome/free-solid-svg-icons';
@Component({
  selector: 'card31',
  templateUrl: './card31.component.html',
  styleUrls: ['./card31.component.css']
})
export class Card31Component implements OnInit {

  parameter;

  //icons
  faCheck = faCheck;
  faTimes = faTimes;
  faPaperPlane = faPaperPlane;
  faPlaneDeparture = faPlaneDeparture;
  faRocket = faRocket;

  constructor() { }

  ngOnInit() {
    this.parameter=new Parameter().parameter[0];
  }

}
