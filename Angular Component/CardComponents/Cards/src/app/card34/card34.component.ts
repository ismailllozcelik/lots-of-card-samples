import { Component, OnInit } from '@angular/core';
import { Parameter } from './parameter';
import { faEnvelope, faMapMarker, faPhoneAlt, faMapMarkedAlt } from '@fortawesome/free-solid-svg-icons';
@Component({
  selector: 'card34',
  templateUrl: './card34.component.html',
  styleUrls: ['./card34.component.css']
})
export class Card34Component implements OnInit {

  parameter;
  //icons
  faEnvelope = faEnvelope;
  faMapMarker = faMapMarkedAlt;
  faPhoneAlt = faPhoneAlt;

  constructor() { }

  ngOnInit() {
    this.parameter = new Parameter().parameter[0];
  }

}
