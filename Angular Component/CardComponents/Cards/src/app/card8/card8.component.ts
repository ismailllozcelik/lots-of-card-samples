import { Component, OnInit } from '@angular/core';
import {Parameter} from './parameter';
@Component({
  selector: 'card8',
  templateUrl: './card8.component.html',
  styleUrls: ['./card8.component.css']
})
export class Card8Component implements OnInit {

  parameter;

  constructor() { }

  ngOnInit() {
    this.parameter=new Parameter().parameter[0];
  }

}
