import { Component, OnInit } from '@angular/core';
import { Parameter } from './parameter';
import { faUser, faCommentDots, faClock } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'card23',
  templateUrl: './card23.component.html',
  styleUrls: ['./card23.component.css']
})
export class Card23Component implements OnInit {

  parameter;

  //icons
  faUser = faUser;
  faClock = faClock;
  faCommentDots = faCommentDots;

  constructor() { }

  ngOnInit() {
    this.parameter = new Parameter().parameter[0];
  }

}
