import { Component, OnInit } from '@angular/core';
import { Parameter } from './parameter';
@Component({
  selector: 'card12',
  templateUrl: './card12.component.html',
  styleUrls: ['./card12.component.css']
})
export class Card12Component implements OnInit {

  parameter;
  constructor() { }

  ngOnInit() {
    this.parameter = new Parameter().parameter[0];
  }

}
