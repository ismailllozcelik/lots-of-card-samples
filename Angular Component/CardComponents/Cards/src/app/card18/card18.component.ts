import { Component, OnInit } from '@angular/core';
import { Parameter } from './parameter';
import { faFacebook, faTwitter, faInstagram, faGooglePlus, faLinkedinIn } from '@fortawesome/free-brands-svg-icons';
@Component({
  selector: 'card18',
  templateUrl: './card18.component.html',
  styleUrls: ['./card18.component.css']
})
export class Card18Component implements OnInit {

  parameter;

  //icons
  faFacebook = faFacebook;
  faTwitter = faTwitter;
  faInstagram = faInstagram;
  faGooglePlus = faGooglePlus;
  faLinkedinIn = faLinkedinIn;

  constructor() { }

  ngOnInit() {
    this.parameter = new Parameter().parameter[0];
  }

}
