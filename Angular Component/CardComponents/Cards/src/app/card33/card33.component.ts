import { Component, OnInit } from '@angular/core';
import { Parameter } from './parameter';
import { faFacebook, faTwitter, faLinkedin, faInstagram, faGooglePlusG } from '@fortawesome/free-brands-svg-icons';
@Component({
  selector: 'card33',
  templateUrl: './card33.component.html',
  styleUrls: ['./card33.component.css']
})
export class Card33Component implements OnInit {

  parameter;

  //icons
  faFacebook = faFacebook;
  faTwitter = faTwitter;
  faLinkedin = faLinkedin;
  faInstagram = faInstagram;
  faGooglePlusG = faGooglePlusG;

  constructor() { }

  ngOnInit() {
    this.parameter = new Parameter().parameter[0];
  }

}
