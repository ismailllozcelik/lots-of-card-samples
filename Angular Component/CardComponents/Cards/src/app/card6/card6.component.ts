import { Component, OnInit } from '@angular/core';
import {Parameter} from './parameter';
@Component({
  selector: 'card6',
  templateUrl: './card6.component.html',
  styleUrls: ['./card6.component.css']
})
export class Card6Component implements OnInit {

  parameter;
  constructor() { }

  ngOnInit() {
    this.parameter=new Parameter().parameter[0];
  }

}
