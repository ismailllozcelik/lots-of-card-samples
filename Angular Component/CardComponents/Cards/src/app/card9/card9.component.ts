import { Component, OnInit } from '@angular/core';
import { Parameter } from './parameter';
import { faStar, faStarHalf } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'card9',
  templateUrl: './card9.component.html',
  styleUrls: ['./card9.component.css']
})
export class Card9Component implements OnInit {

  parameter;
  faStar = faStar;
  faTwitter = faStarHalf;
  constructor() { }

  ngOnInit() {
    this.parameter = new Parameter().parameter[0];
  }

}
