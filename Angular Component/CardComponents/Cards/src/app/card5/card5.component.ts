import { Component, OnInit } from '@angular/core';
import {Parameter} from './parameter';
@Component({
  selector: 'card5',
  templateUrl: './card5.component.html',
  styleUrls: ['./card5.component.css']
})
export class Card5Component implements OnInit {

  parameter;
  constructor() { }

  ngOnInit() {
    this.parameter=new Parameter().parameter[0];
  }

}
