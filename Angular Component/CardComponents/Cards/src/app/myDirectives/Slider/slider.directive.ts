import { Directive, ElementRef, HostListener } from '@angular/core';
import * as $ from 'jquery';


@Directive({
  selector: '[sliderIconClick]'
})
export class SliderDirective {

  //hangi elemana tıklandığının referansını alıyoruz
  constructor(private elementRef:ElementRef) {}



  @HostListener('click') slide(){

    if(this.elementRef.nativeElement.classList.value=="next")
    {
      $('.slide').find('.active').next().addClass('active');
      $(".slide").find(".active").prev().removeClass("active");

    }

    if(this.elementRef.nativeElement.classList.value=="prev")
    {
      $(".slide").find(".active").prev().addClass("active");
      $(".slide").find(".active").next().removeClass("active");
    }
  //console.log( );
  }






}