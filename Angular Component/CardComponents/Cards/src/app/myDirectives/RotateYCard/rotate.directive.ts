import { Directive, HostListener, ElementRef, Input } from '@angular/core';
import * as $ from 'jquery';

@Directive({
  selector: '[RotateYCard]'
})
export class RotateDirective {

  constructor(private elementRef: ElementRef) { }

 
  @HostListener('mousemove') rotateY() {

 
    $("#"+this.elementRef.nativeElement.id).mousemove(function 
      (event) {

        $("#"+event.currentTarget.children[0].id)[0].style.transform="rotateY("+event.pageX/4+"deg)";
       
    });

  }

  }


