import { Directive, Input, ElementRef, OnInit } from '@angular/core';

@Directive({
  selector: '[appAddDiv]'
})
export class AddDivDirective implements OnInit {
  

@Input() Parameter;

  constructor(private elementRef:ElementRef) { }

  ngOnInit(): void {
   let div=
        `
        <div class="alert alert-success text-center">
        ID : ${this.Parameter.id} - STYLE : ${this.Parameter.style}
        </div>
        `;

    this.elementRef.nativeElement.innerHTML=div;
  }

   
}
