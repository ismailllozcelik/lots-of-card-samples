import { Directive, OnInit, ElementRef } from '@angular/core';
import * as $ from 'jquery';
@Directive({
  selector: '[appChangeImg]'
})
export class ChangeImgDirective implements OnInit{
 
  constructor(private elementRef:ElementRef) { }

  ngOnInit(): void {

   
      $(this.elementRef.nativeElement.children).click(function(e){

        e.currentTarget.previousElementSibling.src=e.target.src;
       
      });
          
        
    
    }

 
  }


