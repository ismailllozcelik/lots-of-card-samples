import { Component, OnInit } from '@angular/core';
import { Parameter } from './parameter';
import { faFacebook, faTwitter, faLinkedinIn, faInstagram, faGooglePlus } from '@fortawesome/free-brands-svg-icons';
@Component({
  selector: 'card29',
  templateUrl: './card29.component.html',
  styleUrls: ['./card29.component.css']
})
export class Card29Component implements OnInit {

  parameter;

  //icons
  faFacebook = faFacebook;
  faTwitter = faTwitter;
  faLinkedinIn = faLinkedinIn;
  faInstagram = faInstagram;
  faGooglePlus = faGooglePlus;

  constructor() { }

  ngOnInit() {
    this.parameter = new Parameter().parameter[0];
  }

}
