import { Component, OnInit } from '@angular/core';
import { Parameter } from './parameter';
import { faFacebook, faTwitter, faLinkedin } from '@fortawesome/free-brands-svg-icons';


@Component({
  selector: 'card35',
  templateUrl: './card35.component.html',
  styleUrls: ['./card35.component.css']
})
export class Card35Component implements OnInit {

  parameter;
  faFacebook = faFacebook;
  faTwitter = faTwitter;
  faLinkedin = faLinkedin;

  constructor() { }


  tiltSettings() {

    return {
      max: 10,
      glare: true,
      "max-glare": .5
    }
  }




  ngOnInit() {
    this.parameter = new Parameter().parameter[0];
  }


}
