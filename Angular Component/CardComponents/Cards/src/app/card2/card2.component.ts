import { Component, OnInit } from '@angular/core';
import {Parameter} from './parameter';

@Component({
  selector: 'card2',
  templateUrl: './card2.component.html',
  styleUrls: ['./card2.component.css']
})
export class Card2Component implements OnInit {

  parameter;
  constructor() { }

  ngOnInit() {
    this.parameter= new Parameter().parameter[0];
  }

}
