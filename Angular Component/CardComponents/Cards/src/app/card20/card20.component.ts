import { Component, OnInit } from '@angular/core';
import { Parameter } from './parameter';
import { faTimes, faCheck, faRocket, faPaperPlane, faPlaneDeparture } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'card20',
  templateUrl: './card20.component.html',
  styleUrls: ['./card20.component.css']
})
export class Card20Component implements OnInit {

  parameter;

  faTimes = faTimes;
  faCheck = faCheck;
  faPlaneDeparture = faPlaneDeparture;
  faRocket = faRocket;
  faPaperPlane = faPaperPlane;

  constructor() { }

  ngOnInit() {
    this.parameter = new Parameter().parameter[0];
  }

}
