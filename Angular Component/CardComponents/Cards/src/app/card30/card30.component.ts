import { Component, OnInit } from '@angular/core';
import { Parameter } from './parameter';
import { faFacebook, faTwitter, faInstagram, faLinkedinIn, faGooglePlusG } from '@fortawesome/free-brands-svg-icons';
@Component({
  selector: 'card30',
  templateUrl: './card30.component.html',
  styleUrls: ['./card30.component.css']
})
export class Card30Component implements OnInit {

  parameter;

  //icons
  faFacebook = faFacebook;
  faTwitter = faTwitter;
  faInstagram = faInstagram;
  faLinkedinIn = faLinkedinIn;
  faGooglePlusG = faGooglePlusG;

  constructor() { }

  ngOnInit() {
    this.parameter = new Parameter().parameter[0];
  }

}
