import { Component, OnInit } from '@angular/core';
import { Parameter } from './parameter';
@Component({
  selector: 'card14',
  templateUrl: './card14.component.html',
  styleUrls: ['./card14.component.css']
})
export class Card14Component implements OnInit {


  parameter;

  constructor() { }

  ngOnInit() {
    this.parameter = new Parameter().parameter[0];
  }

}
