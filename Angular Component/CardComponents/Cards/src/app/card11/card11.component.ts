import { Component, OnInit } from '@angular/core';
import { Parameter } from './parameter';
import { faFacebook, faTwitter, faLinkedin } from '@fortawesome/free-brands-svg-icons';


@Component({
  selector: 'card11',
  templateUrl: './card11.component.html',
  styleUrls: ['./card11.component.css']
})
export class Card11Component implements OnInit {

  parameter;
  faFacebook = faFacebook;
  faTwitter = faTwitter;
  faLinkedin = faLinkedin;

  constructor() { }

  ngOnInit() {
    this.parameter = new Parameter().parameter[0];
  }


}
