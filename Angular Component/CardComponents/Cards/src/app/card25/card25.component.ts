import { Component, OnInit } from '@angular/core';
import { Parameter } from './parameter';
import { faQuoteLeft } from '@fortawesome/free-solid-svg-icons';
@Component({
  selector: 'card25',
  templateUrl: './card25.component.html',
  styleUrls: ['./card25.component.css']
})
export class Card25Component implements OnInit {

  parameter;

  //icon
  faQuoteLeft = faQuoteLeft;

  constructor() { }

  ngOnInit() {
    this.parameter = new Parameter().parameter[0];
  }

}
