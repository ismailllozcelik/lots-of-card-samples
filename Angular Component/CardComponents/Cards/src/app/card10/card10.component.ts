import { Component, OnInit } from '@angular/core';
import {Parameter} from './parameter';
import { faPaperPlane, faPlaneDeparture, faRocket ,faCheck,faTimes} from '@fortawesome/free-solid-svg-icons';
@Component({
  selector: 'card10',
  templateUrl: './card10.component.html',
  styleUrls: ['./card10.component.css']
})
export class Card10Component implements OnInit {

  parameter;
  faPaperPlane=faPaperPlane;
  faRocket=faRocket;
  faPlaneDeparture=faPlaneDeparture;
  faCheck=faCheck;
  faTimes=faTimes;
  constructor() { }

  ngOnInit() {
    this.parameter= new Parameter().parameter[0];
  }

}
