import { Component, OnInit } from '@angular/core';
import {Parameter} from './parameter';

@Component({
  selector: 'card1',
  templateUrl: './card1.component.html',
  styleUrls: ['./card1.component.css']
})
export class Card1Component implements OnInit {
  
  parameter;
  constructor() { }

  ngOnInit() {
    this.parameter=new Parameter().parameter[0];
  }

  

}
