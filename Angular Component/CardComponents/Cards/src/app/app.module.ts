import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { CardsComponent } from './cards/cards.component';
import { Card1Component } from './card1/card1.component';
import { Card2Component } from './card2/card2.component';
import { Card3Component } from './card3/card3.component';
import { Card4Component } from './card4/card4.component';
import { Card5Component } from './card5/card5.component';
import { Card6Component } from './card6/card6.component';
import { Card7Component } from './card7/card7.component';
import { Card8Component } from './card8/card8.component';
import { Card9Component } from './card9/card9.component';
import { Card10Component } from './card10/card10.component';
import { Card11Component } from './card11/card11.component';
import { Card12Component } from './card12/card12.component';
import { Card13Component } from './card13/card13.component';
import { Card14Component } from './card14/card14.component';
import { Card15Component } from './card15/card15.component';
import { Card16Component } from './card16/card16.component';
import { Card17Component } from './card17/card17.component';
import { Card18Component } from './card18/card18.component';
import { Card19Component } from './card19/card19.component';
import { Card20Component } from './card20/card20.component';
import { Card21Component } from './card21/card21.component';
import { Card22Component } from './card22/card22.component';
import { Card23Component } from './card23/card23.component';
import { Card24Component } from './card24/card24.component';
import { Card25Component } from './card25/card25.component';
import { Card26Component } from './card26/card26.component';
import { Card27Component } from './card27/card27.component';
import { Card28Component } from './card28/card28.component';
import { Card29Component } from './card29/card29.component';
import { Card30Component } from './card30/card30.component';
import { Card31Component } from './card31/card31.component';
import { Card32Component } from './card32/card32.component';
import { Card33Component } from './card33/card33.component';
import { Card34Component } from './card34/card34.component';
import { Card35Component } from './card35/card35.component';
import {AngularTiltModule} from 'angular-tilt';
import { Card36Component } from './card36/card36.component';
import { Card37Component } from './card37SAS/card37/card37.component';
import { Card38Component } from './card37SAS/card38/card38.component';
import { Card39Component } from './card37SAS/card39/card39.component';
import { SliderDirective } from './myDirectives/Slider/slider.directive';
import { Card40Component } from './card37SAS/card40/card40.component';
import { Card41Component } from './card37SAS/card41/card41.component';
import { RotateDirective } from './myDirectives/RotateYCard/rotate.directive';
import { Card42Component } from './card37SAS/card42/card42.component';
import { Card43Component } from './card37SAS/card43/card43.component';
import { Card44Component } from './card37SAS/card44/card44.component';
import { Card45Component } from './card37SAS/card45/card45.component';

import { UcDSwiperDirective } from './myDirectives/UcdSwiper/uc-dswiper.directive';
import { Card46Component } from './card37SAS/card46/card46.component';
import { Card47Component } from './card37SAS/card47/card47.component';
import { Card48Component } from './card37SAS/card48/card48.component';
import { Card49Component } from './card37SAS/card49/card49.component';
import { Card50Component } from './card37SAS/card50/card50.component';
import { Card51Component } from './card37SAS/card51/card51.component';
import { Card52Component } from './card37SAS/card52/card52.component';
import { Card53Component } from './card37SAS/card53/card53.component';
import { AddDivDirective } from './myDirectives/addDivDirective/add-div.directive';
import { Card54Component } from './card37SAS/card54/card54.component';
import { Card55Component } from './card37SAS/card55/card55.component';
import { Card56Component } from './card37SAS/card56/card56.component';
import { Card57Component } from './card37SAS/card57/card57.component';
import { Card58Component } from './card37SAS/card58/card58.component';
import { CarouselDirectiveDirective } from './myDirectives/CustomCarousel/carousel-directive.directive';
import { Card59Component } from './card37SAS/card59/card59.component';
import { Card60Component } from './card37SAS/card60/card60.component';
import { Card61Component } from './card37SAS/card61/card61.component';
import { Card62Component } from './card37SAS/card62/card62.component';
import { Card63Component } from './card37SAS/card63/card63.component';
import { Card64Component } from './card37SAS/card64/card64.component';
import { Card65Component } from './card37SAS/card65/card65.component';
import { ChangeImgDirective } from './myDirectives/changeImgSrc/change-img.directive';
import { Card66Component } from './card37SAS/card66/card66.component';




@NgModule({
  declarations: [
    AppComponent,
    CardsComponent,
    Card1Component,
    Card2Component,
    Card3Component,
    Card4Component,
    Card5Component,
    Card6Component,
    Card7Component,
    Card8Component,
    Card9Component,
    Card10Component,
    Card11Component,
    Card12Component,
    Card13Component,
    Card14Component,
    Card15Component,
    Card16Component,
    Card17Component,
    Card18Component,
    Card19Component,
    Card20Component,
    Card21Component,
    Card22Component,
    Card23Component,
    Card24Component,
    Card25Component,
    Card26Component,
    Card27Component,
    Card28Component,
    Card29Component,
    Card30Component,
    Card31Component,
    Card32Component,
    Card33Component,
    Card34Component,
    Card35Component,
    Card36Component,
    Card37Component,
    Card38Component,
    Card39Component,
    SliderDirective,
    Card40Component,
    Card41Component,
    RotateDirective,
    Card42Component,
    Card43Component,
    Card44Component,
    Card45Component,
    UcDSwiperDirective,
    Card46Component,
    Card47Component,
    Card48Component,
    Card49Component,
    Card50Component,
    Card51Component,
    Card52Component,
    Card53Component,
    AddDivDirective,
    Card54Component,
    Card55Component,
    Card56Component,
    Card57Component,
    Card58Component,
    CarouselDirectiveDirective,
    Card59Component,
    Card60Component,
    Card61Component,
    Card62Component,
    Card63Component,
    Card64Component,
    Card65Component,
    ChangeImgDirective,
    Card66Component
 

  ],
  imports: [
    BrowserModule,
    FontAwesomeModule,
    AngularTiltModule
  ],
  providers: [
   
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
