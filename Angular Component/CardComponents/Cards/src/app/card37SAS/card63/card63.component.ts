import { Component, OnInit } from '@angular/core';
import { Parameter } from './parameter';
import { faFacebookSquare, faTwitterSquare, faGooglePlusSquare } from '@fortawesome/free-brands-svg-icons';
@Component({
  selector: 'card63',
  templateUrl: './card63.component.html',
  styleUrls: ['./card63.component.css']
})
export class Card63Component implements OnInit {

  parameter;
  faFacebookSquare = faFacebookSquare;
  faTwitterSquare = faTwitterSquare;
  faGooglePlusSquare = faGooglePlusSquare;
  constructor() { }

  ngOnInit() {
    this.parameter = new Parameter().parameter[0];
  }

}
