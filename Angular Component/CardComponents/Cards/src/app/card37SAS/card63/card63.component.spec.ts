import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Card63Component } from './card63.component';

describe('Card63Component', () => {
  let component: Card63Component;
  let fixture: ComponentFixture<Card63Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Card63Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Card63Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
