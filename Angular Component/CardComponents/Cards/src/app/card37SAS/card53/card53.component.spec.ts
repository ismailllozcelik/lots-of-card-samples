import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Card53Component } from './card53.component';

describe('Card53Component', () => {
  let component: Card53Component;
  let fixture: ComponentFixture<Card53Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Card53Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Card53Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
