import { Component, OnInit } from '@angular/core';
import { Parameter } from './parameter';
@Component({
  selector: 'card40',
  templateUrl: './card40.component.html',
  styleUrls: ['./card40.component.css']
})
export class Card40Component implements OnInit {

  parameter;

  constructor() { }

  ngOnInit() {
    this.parameter = new Parameter().parameter[0];
  }

}
