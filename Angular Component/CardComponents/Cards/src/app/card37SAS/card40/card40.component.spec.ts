import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Card40Component } from './card40.component';

describe('Card40Component', () => {
  let component: Card40Component;
  let fixture: ComponentFixture<Card40Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Card40Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Card40Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
