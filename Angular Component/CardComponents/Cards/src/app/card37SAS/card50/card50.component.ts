import { Component, OnInit } from '@angular/core';
import { Parameter } from './parameter';
@Component({
  selector: 'card50',
  templateUrl: './card50.component.html',
  styleUrls: ['./card50.component.css']
})
export class Card50Component implements OnInit {

  parameter;

  constructor() { }

  ngOnInit() {
    this.parameter = new Parameter().parameter[0];
  }

}
