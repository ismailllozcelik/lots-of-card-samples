import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Card50Component } from './card50.component';

describe('Card50Component', () => {
  let component: Card50Component;
  let fixture: ComponentFixture<Card50Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Card50Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Card50Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
