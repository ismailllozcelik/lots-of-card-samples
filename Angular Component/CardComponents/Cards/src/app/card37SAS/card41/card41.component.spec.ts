import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Card41Component } from './card41.component';

describe('Card41Component', () => {
  let component: Card41Component;
  let fixture: ComponentFixture<Card41Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Card41Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Card41Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
