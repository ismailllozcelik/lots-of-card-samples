import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Card65Component } from './card65.component';

describe('Card65Component', () => {
  let component: Card65Component;
  let fixture: ComponentFixture<Card65Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Card65Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Card65Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
