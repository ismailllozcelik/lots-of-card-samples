import { Component, OnInit } from '@angular/core';
import {Parameter} from './parameter';
@Component({
  selector: 'card54',
  templateUrl: './card54.component.html',
  styleUrls: ['./card54.component.css']
})
export class Card54Component implements OnInit {

  parameter;
  constructor() { }

  ngOnInit() {
    this.parameter=new Parameter().parameter[0];
  }

}
