import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Card54Component } from './card54.component';

describe('Card54Component', () => {
  let component: Card54Component;
  let fixture: ComponentFixture<Card54Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Card54Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Card54Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
