import { Component, OnInit } from '@angular/core';
import { Parameter } from './parameter';
@Component({
  selector: 'card47',
  templateUrl: './card47.component.html',
  styleUrls: ['./card47.component.css']
})
export class Card47Component implements OnInit {

  parameter;

  constructor() { }

  ngOnInit() {
    this.parameter = new Parameter().parameter[0];
  }

}
