import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Card47Component } from './card47.component';

describe('Card47Component', () => {
  let component: Card47Component;
  let fixture: ComponentFixture<Card47Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Card47Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Card47Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
