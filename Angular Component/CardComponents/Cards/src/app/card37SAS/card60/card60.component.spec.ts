import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Card60Component } from './card60.component';

describe('Card60Component', () => {
  let component: Card60Component;
  let fixture: ComponentFixture<Card60Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Card60Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Card60Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
