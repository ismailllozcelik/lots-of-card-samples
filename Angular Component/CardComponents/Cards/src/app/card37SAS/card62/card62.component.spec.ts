import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Card62Component } from './card62.component';

describe('Card62Component', () => {
  let component: Card62Component;
  let fixture: ComponentFixture<Card62Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Card62Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Card62Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
