import { Component, OnInit } from '@angular/core';
import { Parameter } from './parameter';
import { faFacebookF, faTwitter, faGooglePlusG, faYoutube } from '@fortawesome/free-brands-svg-icons';
@Component({
  selector: 'card62',
  templateUrl: './card62.component.html',
  styleUrls: ['./card62.component.css']
})
export class Card62Component implements OnInit {

  parameter;
  faFacebookF = faFacebookF;
  faTwitter = faTwitter;
  faGooglePlusG = faGooglePlusG;
  faYoutube = faYoutube;
  constructor() { }

  ngOnInit() {
    this.parameter = new Parameter().parameter[0];
  }

}
