import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Card55Component } from './card55.component';

describe('Card55Component', () => {
  let component: Card55Component;
  let fixture: ComponentFixture<Card55Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Card55Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Card55Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
