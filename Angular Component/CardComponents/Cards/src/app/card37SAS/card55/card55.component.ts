import { Component, OnInit } from '@angular/core';
import {Parameter} from './parameter';
@Component({
  selector: 'card55',
  templateUrl: './card55.component.html',
  styleUrls: ['./card55.component.css']
})
export class Card55Component implements OnInit {

  parameter;
  constructor() { }

  ngOnInit() {
    this.parameter= new Parameter().parameter[0];
  }

}
