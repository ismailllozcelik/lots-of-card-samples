import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Card43Component } from './card43.component';

describe('Card43Component', () => {
  let component: Card43Component;
  let fixture: ComponentFixture<Card43Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Card43Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Card43Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
