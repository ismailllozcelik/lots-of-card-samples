import { Component, OnInit } from '@angular/core';
import { Parameter } from './parameter';
import { faFacebook, faInstagram, faTwitter, faDribbble } from '@fortawesome/free-brands-svg-icons';
@Component({
  selector: 'card59',
  templateUrl: './card59.component.html',
  styleUrls: ['./card59.component.css']
})
export class Card59Component implements OnInit {

  parameter;
  faFacebook = faFacebook;
  faInstagram = faInstagram;
  faTwitter = faTwitter;
  faDribbble = faDribbble;
  constructor() { }

  ngOnInit() {
    this.parameter = new Parameter().parameter[0];
  }

}
