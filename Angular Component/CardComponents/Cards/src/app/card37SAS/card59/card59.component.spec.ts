import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Card59Component } from './card59.component';

describe('Card59Component', () => {
  let component: Card59Component;
  let fixture: ComponentFixture<Card59Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Card59Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Card59Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
