import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Card58Component } from './card58.component';

describe('Card58Component', () => {
  let component: Card58Component;
  let fixture: ComponentFixture<Card58Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Card58Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Card58Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
