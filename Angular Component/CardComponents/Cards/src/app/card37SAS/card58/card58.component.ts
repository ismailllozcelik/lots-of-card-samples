import { Component, OnInit } from '@angular/core';
import { Parameter } from './parameter';
@Component({
  selector: 'card58',
  templateUrl: './card58.component.html',
  styleUrls: ['./card58.component.css']
})
export class Card58Component implements OnInit {

  parameter;
  constructor() { }

  ngOnInit() {
    this.parameter = new Parameter().parameter[0];
  }

}
