import { Component, OnInit } from '@angular/core';
import { Parameter } from './parameter';
import { faFacebookF, faTwitter, faInstagram } from '@fortawesome/free-brands-svg-icons';
import { faEllipsisV } from '@fortawesome/free-solid-svg-icons';
@Component({
  selector: 'card64',
  templateUrl: './card64.component.html',
  styleUrls: ['./card64.component.css']
})
export class Card64Component implements OnInit {

  parameter;
  faFacebookF = faFacebookF;
  faTwitter = faTwitter;
  faInstagram = faInstagram;
  faEllipsisV = faEllipsisV;
  constructor() { }

  ngOnInit() {
    this.parameter = new Parameter().parameter[0];

  }

}
