import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Card64Component } from './card64.component';

describe('Card64Component', () => {
  let component: Card64Component;
  let fixture: ComponentFixture<Card64Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Card64Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Card64Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
