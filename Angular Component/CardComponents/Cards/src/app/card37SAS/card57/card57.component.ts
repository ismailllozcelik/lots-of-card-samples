import { Component, OnInit } from '@angular/core';
import { Parameter } from './parameter';
@Component({
  selector: 'card57',
  templateUrl: './card57.component.html',
  styleUrls: ['./card57.component.css']
})
export class Card57Component implements OnInit {

  parameter;
  constructor() { }

  ngOnInit() {
    this.parameter = new Parameter().parameter[0];
  }

}
