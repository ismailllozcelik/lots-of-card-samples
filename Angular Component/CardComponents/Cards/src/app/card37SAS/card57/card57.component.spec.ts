import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Card57Component } from './card57.component';

describe('Card57Component', () => {
  let component: Card57Component;
  let fixture: ComponentFixture<Card57Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Card57Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Card57Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
