import { Component, OnInit } from '@angular/core';
import { Parameter } from './parameter';
@Component({
  selector: 'card46',
  templateUrl: './card46.component.html',
  styleUrls: ['./card46.component.css']
})
export class Card46Component implements OnInit {

  parameter;


  constructor() { }

  ngOnInit() {
    this.parameter = new Parameter().parameter[0];
  }

}
