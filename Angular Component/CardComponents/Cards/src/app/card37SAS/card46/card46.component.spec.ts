import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Card46Component } from './card46.component';

describe('Card46Component', () => {
  let component: Card46Component;
  let fixture: ComponentFixture<Card46Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Card46Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Card46Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
