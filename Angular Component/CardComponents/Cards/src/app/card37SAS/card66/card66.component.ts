import { Component, OnInit } from '@angular/core';
import { Parameter } from './parameter';
import { faFacebookF, faTwitter, faYoutube, faInstagram, faWhatsapp } from '@fortawesome/free-brands-svg-icons';
@Component({
  selector: 'card66',
  templateUrl: './card66.component.html',
  styleUrls: ['./card66.component.css']
})
export class Card66Component implements OnInit {

  parameter;
  faFacebookF = faFacebookF;
  faTwitter = faTwitter;
  faYoutube = faYoutube;
  faInstagram = faInstagram;
  faWhatsapp = faWhatsapp;
  constructor() { }

  ngOnInit() {
    this.parameter = new Parameter().parameter[0];
  }

}
