import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Card66Component } from './card66.component';

describe('Card66Component', () => {
  let component: Card66Component;
  let fixture: ComponentFixture<Card66Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Card66Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Card66Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
