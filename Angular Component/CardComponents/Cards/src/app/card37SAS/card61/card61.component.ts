import { Component, OnInit } from '@angular/core';
import { Parameter } from './parameter';
import { faFacebook, faTwitter, faWhatsapp } from '@fortawesome/free-brands-svg-icons';
@Component({
  selector: 'card61',
  templateUrl: './card61.component.html',
  styleUrls: ['./card61.component.css']
})
export class Card61Component implements OnInit {

  parameter;
  faFacebook = faFacebook;
  faTwitter = faTwitter;
  faWhatsapp = faWhatsapp;
  constructor() { }

  ngOnInit() {
    this.parameter = new Parameter().parameter[0];
  }

}
