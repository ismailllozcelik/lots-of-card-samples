import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Card61Component } from './card61.component';

describe('Card61Component', () => {
  let component: Card61Component;
  let fixture: ComponentFixture<Card61Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Card61Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Card61Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
