import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Card42Component } from './card42.component';

describe('Card42Component', () => {
  let component: Card42Component;
  let fixture: ComponentFixture<Card42Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Card42Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Card42Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
