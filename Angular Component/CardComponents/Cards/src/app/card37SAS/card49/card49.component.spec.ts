import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Card49Component } from './card49.component';

describe('Card49Component', () => {
  let component: Card49Component;
  let fixture: ComponentFixture<Card49Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Card49Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Card49Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
