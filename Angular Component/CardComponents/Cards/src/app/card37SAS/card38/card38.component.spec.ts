import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Card38Component } from './card38.component';

describe('Card38Component', () => {
  let component: Card38Component;
  let fixture: ComponentFixture<Card38Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Card38Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Card38Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
