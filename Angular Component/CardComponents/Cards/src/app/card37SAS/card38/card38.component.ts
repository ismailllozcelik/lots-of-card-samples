import { Component, OnInit } from '@angular/core';
import { Parameter } from './parameter';

@Component({
  selector: 'card38',
  templateUrl: './card38.component.html',
  styleUrls: ['./card38.component.css']
})
export class Card38Component implements OnInit {

  parameter;

  constructor() { }

  ngOnInit() {
    this.parameter = new Parameter().parameter[0];
  }

}
