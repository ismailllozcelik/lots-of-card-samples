import { Component, OnInit } from '@angular/core';
import { Parameter } from './parameter';
import { from } from 'rxjs';
@Component({
  selector: 'card45',
  templateUrl: './card45.component.html',
  styleUrls: ['./card45.component.css']
})
export class Card45Component implements OnInit {

  parameter;
  constructor() { }

  ngOnInit() {
    this.parameter = new Parameter().parameter[0];
  }

}
