import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Card45Component } from './card45.component';

describe('Card45Component', () => {
  let component: Card45Component;
  let fixture: ComponentFixture<Card45Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Card45Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Card45Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
