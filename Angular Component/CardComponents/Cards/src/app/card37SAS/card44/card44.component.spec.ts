import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Card44Component } from './card44.component';

describe('Card44Component', () => {
  let component: Card44Component;
  let fixture: ComponentFixture<Card44Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Card44Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Card44Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
