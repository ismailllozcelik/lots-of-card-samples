import { Component, OnInit } from '@angular/core';
import { Parameter } from './parameter';
@Component({
  selector: 'card44',
  templateUrl: './card44.component.html',
  styleUrls: ['./card44.component.css']
})
export class Card44Component implements OnInit {

  parameter;

  constructor() { }

  ngOnInit() {
    this.parameter = new Parameter().parameter[0];
  }

}
