import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Card39Component } from './card39.component';

describe('Card39Component', () => {
  let component: Card39Component;
  let fixture: ComponentFixture<Card39Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Card39Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Card39Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
