import { Component, OnInit } from '@angular/core';
import { Parameter } from './parameter';
import { faAngleLeft, faAngleRight } from '@fortawesome/free-solid-svg-icons';
@Component({
  selector: 'card39',
  templateUrl: './card39.component.html',
  styleUrls: ['./card39.component.css']
})
export class Card39Component implements OnInit {

  parameter;

  //next. prev icons
  faAngleLeft = faAngleLeft;
  faAngleRight = faAngleRight;


  constructor() { }

  ngOnInit() {
    this.parameter = new Parameter().parameter[0];
  }

}
