import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Card51Component } from './card51.component';

describe('Card51Component', () => {
  let component: Card51Component;
  let fixture: ComponentFixture<Card51Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Card51Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Card51Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
