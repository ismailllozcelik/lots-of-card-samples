import { Component, OnInit } from '@angular/core';
import { Parameter } from './parameter';
@Component({
  selector: 'card51',
  templateUrl: './card51.component.html',
  styleUrls: ['./card51.component.css']
})
export class Card51Component implements OnInit {

  parameter;


  constructor() { }

  ngOnInit() {
    this.parameter = new Parameter().parameter[0];
  }

}
