import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Card37Component } from './card37.component';

describe('Card37Component', () => {
  let component: Card37Component;
  let fixture: ComponentFixture<Card37Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Card37Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Card37Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
