import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Card56Component } from './card56.component';

describe('Card56Component', () => {
  let component: Card56Component;
  let fixture: ComponentFixture<Card56Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Card56Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Card56Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
