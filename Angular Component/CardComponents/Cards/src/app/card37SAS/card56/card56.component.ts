import { Component, OnInit } from '@angular/core';
import { Parameter } from './parameter';
@Component({
  selector: 'card56',
  templateUrl: './card56.component.html',
  styleUrls: ['./card56.component.css']
})
export class Card56Component implements OnInit {

  parameter;
  constructor() { }

  ngOnInit() {
    this.parameter = new Parameter().parameter[0];
  }

}
