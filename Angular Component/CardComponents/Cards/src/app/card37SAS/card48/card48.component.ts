import { Component, OnInit } from '@angular/core';
import { Parameter } from './parameter';

@Component({
  selector: 'card48',
  templateUrl: './card48.component.html',
  styleUrls: ['./card48.component.css']
})
export class Card48Component implements OnInit {

  parameter;

  constructor() { }

  ngOnInit() {
    this.parameter = new Parameter().parameter[0];
  }

}
