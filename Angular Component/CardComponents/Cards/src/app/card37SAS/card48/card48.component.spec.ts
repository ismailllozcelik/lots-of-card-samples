import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Card48Component } from './card48.component';

describe('Card48Component', () => {
  let component: Card48Component;
  let fixture: ComponentFixture<Card48Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Card48Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Card48Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
