import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Card52Component } from './card52.component';

describe('Card52Component', () => {
  let component: Card52Component;
  let fixture: ComponentFixture<Card52Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Card52Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Card52Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
