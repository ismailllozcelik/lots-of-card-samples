import { Component, OnInit } from '@angular/core';
import { Parameter } from './parameter';
@Component({
  selector: 'card52',
  templateUrl: './card52.component.html',
  styleUrls: ['./card52.component.css']
})
export class Card52Component implements OnInit {

  parameter;


  constructor() { }

  ngOnInit() {
    this.parameter = new Parameter().parameter[0];
  }

}
