import { Component, OnInit } from '@angular/core';
import { Parameter } from './parameter';
import { faFacebook, faTwitter, faLinkedinIn } from '@fortawesome/free-brands-svg-icons';
@Component({
  selector: 'card13',
  templateUrl: './card13.component.html',
  styleUrls: ['./card13.component.css']
})
export class Card13Component implements OnInit {

  parameter;
  faFacebook = faFacebook;
  faTwitter = faTwitter;
  faLinkedinIn = faLinkedinIn;

  constructor() { }

  ngOnInit() {
    this.parameter = new Parameter().parameter[0];
  }

}
