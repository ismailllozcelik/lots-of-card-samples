import { Component, OnInit } from '@angular/core';
import { Parameter } from './parameter';
import { faFacebook, faTwitter, faGooglePlusG, faInstagram, faLinkedinIn } from '@fortawesome/free-brands-svg-icons';

@Component({
  selector: 'card21',
  templateUrl: './card21.component.html',
  styleUrls: ['./card21.component.css']
})
export class Card21Component implements OnInit {

  parameter;
  //icons

  faFacebook = faFacebook;
  faTwitter = faTwitter;
  faGooglePlusG = faGooglePlusG;
  faInstagram = faInstagram;
  faLinkedinIn = faLinkedinIn;

  constructor() { }

  ngOnInit() {
    this.parameter = new Parameter().parameter[0];
  }

}
